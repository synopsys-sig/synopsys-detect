#!/bin/bash

source "$(dirname "$0")/common.sh"

function runCodeInsightForSCA(){
    PROJECT_NAME=`grep "Project name:" detect_logs.log | sed 's/^.*: //'`
    PROJECT_VERSION=`grep "Project version:" detect_logs.log | sed 's/^.*: //'`

    url_escape() {
    echo `echo $1 | sed s:{:%7B:g | sed s:}:%7D:g`
    }

    PROJECT_NAME=${PROJECT_NAME:?'PROJECT_NAME is empty, make sure the detect scan ran successfully and log messages show Project Name'}
    PROJECT_VERSION=${PROJECT_VERSION:?'PROJECT_VERSION is empty, make sure the detect scan ran successfully and log messages show Project Version'}
    APP_PASSWORD=${APP_PASSWORD:?'APP_PASSWORD is needed to run code insight read more in the documentation.'}

    # Get the JAR & execute it
    sourceDir=$(pwd)
    cd ..
    jarDir=$(pwd)
    wget https://bitbucket-reporting.s3.us-east-2.amazonaws.com/bitbucket-sca-reporting-all-1.0-SNAPSHOT.jar
    cd "$sourceDir" || exit

    java -jar "$jarDir"/bitbucket-sca-reporting-all-1.0-SNAPSHOT.jar \
    $BITBUCKET_REPO_OWNER_UUID \
    $BITBUCKET_REPO_UUID \
    $BITBUCKET_COMMIT \
    $BITBUCKET_REPO_OWNER \
    $APP_PASSWORD \
    $BLACKDUCK_SERVER_URL \
    $BLACKDUCK_ACCESS_TOKEN \
    $PROJECT_NAME \
    $PROJECT_VERSION

    echo "\n Finished Uploading!"
}

function runCodeInsightForPolaris() {
    SUMMARY_URL=$(grep "\"SummaryUrl\":" polaris_logs.log | sed 's/^.*: //'| sed -e 's/^"//' -e 's/"$//')
    PROJECT_NAME=`echo $BITBUCKET_REPO_FULL_NAME  | tr '[:upper:]' '[:lower:]'`
    PROJECT_VERSION=`echo $BITBUCKET_BRANCH  | tr '[:upper:]' '[:lower:]'`

    if [ -n "$SUMMARY_URL" ]; then
      echo "Summary URL ~> $SUMMARY_URL"
    else
      echo "Summary URL not found so trying with project name & branch."
      SUMMARY_URL="null"
    fi

    APP_PASSWORD=${APP_PASSWORD:?'APP_PASSWORD is needed to run code insight read more in the documentation.'}
    echo $APP_PASSWORD

    echo $POLARIS_SERVER_URL
    echo $POLARIS_ACCESS_TOKEN

    # Get the JAR & execute it
    sourceDir=$(pwd)
    cd ..
    jarDir=$(pwd)
    wget https://bitbucket-reporting.s3.us-east-2.amazonaws.com/bitbucket-polaris-reporting-all-1.0-SNAPSHOT.jar
    cd "$sourceDir" || exit

    java -jar "$jarDir"/bitbucket-polaris-reporting-all-1.0-SNAPSHOT.jar \
    $BITBUCKET_REPO_OWNER_UUID \
    $BITBUCKET_REPO_UUID \
    $BITBUCKET_COMMIT \
    $BITBUCKET_REPO_OWNER \
    $APP_PASSWORD \
    $POLARIS_SERVER_URL \
    $POLARIS_ACCESS_TOKEN \
    $PROJECT_NAME \
    $PROJECT_VERSION \
    $SUMMARY_URL

    echo "\n Finished Uploading!"
}

function fileExists() {
    if [ "$( find . -name "$1" | wc -l | sed 's/^ *//' )" == "0" ]; 
    then return 1; 
    else return 0; 
    fi
}

function installPython() {
    apt-get update -y && \
    apt-get install python3-pip idle3 -y && \
    pip3 install --no-cache-dir --upgrade pip && \
    \
    # delete cache and tmp files
    apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/cache/* && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/* && \
    rm -rf /var/lib/apt/lists/* && \
    \
    # make some useful symlinks that are expected to exist
    cd /usr/bin && \
    ln -s idle3 idle && \
    ln -s pydoc3 pydoc && \
    ln -s python3 python && \
    ln -s python3-config python-config && \
    cd /

    wget https://repo.continuum.io/archive/Anaconda3-5.0.1-Linux-x86_64.sh && \
    bash Anaconda3-5.0.1-Linux-x86_64.sh -b && \
    rm Anaconda3-5.0.1-Linux-x86_64.sh

    # Set path to conda
    echo PATH=/root/anaconda3/bin:$PATH
}

if fileExists "Pipfile" || fileExists "Pipfile.lock" || fileExists "setup.py" || fileExists "requirements.txt" || fileExists "environment.yml"; 
then installPython; 
fi

if [ "$CODE_INSIGHT" = true ];
    then WAIT_FOR_RESULTS=false; #change it to true in prod
fi

FAIL_RUN=true
if [ -n "$BLACKDUCK_SERVER_URL" ];
then 
    FAIL_RUN=false
    BLACKDUCK_ACCESS_TOKEN=${BLACKDUCK_ACCESS_TOKEN:?'BLACKDUCK_ACCESS_TOKEN variable missing.'}
    TOOLS=${TOOLS:="SIGNATURE_SCAN,DETECTOR"}
    FORCE_SUCCESS=${FORCE_SUCCESS:="false"}
    FAIL_ON_SEVERITY=${FAIL_ON_SEVERITY:="UNSPECIFIED"}
    LOG_LEVEL=${LOG_LEVEL:="INFO"}
    WAIT_FOR_RESULTS=${WAIT_FOR_RESULTS:="false"}
    CODE_INSIGHT=${CODE_INSIGHT:="false"}
    ADDITIONAL_ARGS=${ADDITIONAL_ARGS:=""}
    bash <(curl -s https://detect.synopsys.com/detect.sh) \
        --blackduck.url="$BLACKDUCK_SERVER_URL" \
        --blackduck.api.token="$BLACKDUCK_ACCESS_TOKEN" \
        --detect.tools="$TOOLS" \
        --detect.force.success="$FORCE_SUCCESS" \
        --detect.wait.for.results="$WAIT_FOR_RESULTS" \
        --detect.policy.check.fail.on.severities="$FAIL_ON_SEVERITY" \
        --logging.level.com.synopsys.integration="$LOG_LEVEL" \
        "$ADDITIONAL_ARGS" 2>&1 | tee detect_logs.log

    if [ "$CODE_INSIGHT" = true ];
    then runCodeInsightForSCA;
    fi
else
    echo "Black Duck URL not found so won't run detect scan."
fi

if [ -n "$POLARIS_SERVER_URL" ]; 
then 
    FAIL_RUN=false
    # required parameters
    POLARIS_ACCESS_TOKEN=${POLARIS_ACCESS_TOKEN:?'POLARIS_ACCESS_TOKEN variable missing.'}
    POLARIS_CONFIG_FILENAME=${POLARIS_CONFIG_FILENAME:?'POLARIS_CONFIG_FILENAME variable missing.'}
    mkdir "/root/.synopsys"
    POLARIS_HOME="/root/.synopsys"
    sourceDir=$(pwd)
    cd $POLARIS_HOME || exit
    POLARIS_DIST=${POLARIS_DIST:="polaris_cli-linux64.zip"}
    wget "$POLARIS_SERVER_URL"/api/tools/"$POLARIS_DIST"
    DIR=$(zipinfo -1 ${POLARIS_DIST} | grep -oE '^[^/]+' | uniq)
    unzip ${POLARIS_DIST}
    rm ${POLARIS_DIST}
    cd "${DIR}"/bin || exit

    if [ -n "$POLARIS_BUILD_IMAGE" ]
    then
        POLARIS_DIR=`pwd`
        docker container run \
            -e "POLARIS_SERVER_URL=$POLARIS_SERVER_URL" \
            -e "POLARIS_ACCESS_TOKEN=$POLARIS_ACCESS_TOKEN" \
            -e "POLARIS_CONFIG_FILENAME=$POLARIS_CONFIG_FILENAME" \
            -e "POLARIS_HOME=$POLARIS_HOME" \
            -v "$BITBUCKET_CLONE_DIR/$PROJECT_FOLDER:/project" \
            -v "$POLARIS_DIR":/usr/bin \
            -d $POLARIS_BUILD_IMAGE sh -c "mkdir /root/.synopsys; cd /project; polaris analyze -w"
    fi

    polaris=$(pwd)/polaris
    cd "$sourceDir" || exit

    if [ -z "$POLARIS_ADDITIONAL_ARGS" ]
    then
        "$polaris" -c "$POLARIS_CONFIG_FILENAME" analyze -w 2>&1 | tee polaris_logs.log
    else
        "$polaris"  -c "$POLARIS_CONFIG_FILENAME" analyze -w "$POLARIS_ADDITIONAL_ARGS" 2>&1 | tee polaris_logs.log
    fi

    if [ "$CODE_INSIGHT" = true ];
    then runCodeInsightForPolaris;
    fi
else
    echo "Polaris URL not found so won't run polaris scan."
fi

if [ "$FAIL_RUN" = true ];
then exit 1
fi