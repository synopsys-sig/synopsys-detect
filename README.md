# Bitbucket Pipelines Pipe: Synopsys
​
Synopsys Pipe allows users to quickly scan their applications for source code defects and open source vulnerabilities to manage and remediate risks early in the development cycle.

* The only solution that brings together both SAST and SCA results
* Leverage a wide variety of configuration options including printed reports, different scanning mechanisms and breaking the pipeline.
* Scan results are ready to be viewed in the product panel for Black Duck and/or Polaris
* Upon opening a Pull Request with Bitbucket Code Insight, view the vulnerability overview in the Bitbucket UI panel and open JIRA tickets.

Synopsys Pipe is easy to implement and streamlines the process for delivering high fidelity results. Made with developers in mind, the users never have to leave the Bitbucket ecosystem to build secure, high-quality code faster.

## YAML Definition

### Invoking Black Duck (SCA)

​
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
​

```yaml
- pipe: synopsys-sig/synopsys-detect:0.1.0
  variables:
    BLACKDUCK_SERVER_URL: "<string>"
    BLACKDUCK_ACCESS_TOKEN: "<string>"
    # TOOLS: "<string>" # Optional
    # FORCE_SUCCESS: "<string>" # Optional.
    # FAIL_ON_SEVERITY: "<string>" # Optional.
    # LOG_LEVEL: "<string>" # Optional.
    # CODE_INSIGHT: "<boolean>" # Optional.
    # APP_PASSWORD: "<string>" # Optional.
    # ADDITIONAL_ARGS: "<string>" # Optional.
```

​

### Black Duck Variables

| Variable            | Usage                                     |
| --------------      | ----------------------------------------- |
| BLACKDUCK_SERVER_URL (\*)     | The Black Duck instance URL. |
| BLACKDUCK_ACCESS_TOKEN (\*)     | The Black Duck API Token. |
| TOOLS    | Select which tool to scan the project with. Default: `SIGNATURE_SCAN,DETECTOR`. |
| FORCE_SUCCESS    | Continue build despite issues found. Default: `false`. |
| FAIL_ON_SEVERITY  | A comma-separated list of policy violation severities that will fail Detect. Allowed Values: `ALL`, `BLOCKER`, `CRITICAL`, `MAJOR`, `MINOR`, `TRIVIAL`, `UNSPECIFIED`, default: `UNSPECIFIED` |
| LOG_LEVEL               | Turn on extra logging information, allowed Values: `TRACE`, `DEBUG`, `INFO`, `MAJOR`, `WARN`, `ERROR`, `FATAL`, default: `OFF` . Default: `INFO`. |
| CODE_INSIGHT               | Turn on to get a codde insight report. Default: `false`. |
| APP_PASSWORD               | If code insight is activated this control password needs to be generated via Bitbucket Settings > Access Management > App Passwords > Create App Password Default: `nil`. |
| ADDITIONAL_ARGS          | Additional arguments to be passed to the synopsys detect cli. Default: none. See [Detect Docs](https://blackducksoftware.github.io/synopsys-detect/latest/) for a  full list of possible args. |

_(\*) = required variable._
​

### Invoking Polaris (SAST)

```yaml
- pipe: synopsys-sig/synopsys-detect:0.1.0
  variables:
    POLARIS_SERVER_URL: "<string>"
    POLARIS_ACCESS_TOKEN: "<string>"
    POLARIS_CONFIG_FILENAME: "<string>"
    # POLARIS_BUILD_IMAGE: "<string>" # optional
    # CODE_INSIGHT: "<boolean>" #optional
    # APP_PASSWORD: "<boolean>" #optional, required if using CODE_INSIGHT
    # POLARIS_ADDITIONAL_ARGS: "<string>" # Optional.
```

​

### Polaris Variables

| Variable            | Usage                                     |
| --------------      | ----------------------------------------- |
| POLARIS_SERVER_URL (\*)     | The Polaris instance URL. |
| POLARIS_ACCESS_TOKEN (\*)     | The Polaris Access Token. |
| POLARIS_CONFIG_FILENAME (\*)     | The yml file name which contains polaris config. |
| POLARIS_BUILD_IMAGE     | The image used to compile & build your project. |
| CODE_INSIGHT               | Turn on to get a codde insight report. Default: `false`. |
| APP_PASSWORD               | If code insight is activated this control password needs to be generated via Bitbucket Settings > Access Management > App Passwords > Create App Password Default: `nil`. |
| POLARIS_ADDITIONAL_ARGS          | Additional arguments to be passed to the polaris cli. Default: none. See help section in your polaris instance for a full list of possible args. |

_(\*) = required variable._
​
​
​
(*) = required variable.
For other optional variables, please see help section in your polaris instance for a full list of possible args.
​

## Details

​
Integrating Synopsys Detect into Bitbucket Pipelines adds application security testing automation in your development processes.  Both Static Application Security Testing, and Software Composition Analysis are available to provide complete security coverage for your applications.
​

## Prerequisites

​

* A licensed version of Synopsys Polaris or Black Duck
* The URL of either the Polaris or Black Duck server
* Either a Polaris or Black Duck API Token
​
​

## Examples

### Basic Black Duck scan example

​
Uses Black Duck to scan a Node.js application and break the build if any vulnerabilities found.
​

```yaml
script:
  - npm install
​
  - npm test
​
  - pipe: synopsys-sig/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
​
  - npm publish
```

### Basic Coverity on Polaris scan example

​
Uses Coverity to scan a Java maven application.

```yaml
script:
  - mvn clean package
​
  - pipe: synopsys-sig/synopsys-detect:0.1.0
    variables:
      POLARIS_SERVER_URL: $POLARIS_SERVER_URL
      POLARIS_ACCESS_TOKEN: $POLARIS_ACCESS_TOKEN
      POLARIS_CONFIG_FILENAME: "polaris.yml"
```

## Advanced example

### Scan and View Report with Code Insight

```yaml
script:
  - npm clean-install

  - npm test

  - pipe: synopsys-sig/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
      FAIL_ON_SEVERITY: "HIGH"
      CODE_INSIGHT: "true"
      APP_PASSWORD: $APP_PASSWORD

  - docker build -t $IMAGE_NAME .

  - docker push $IMAGE_NAME

```

### Fail on high severity and print pdf report

```yaml
script:
  - npm clean-install

  - npm test

  - pipe: synopsys-sig/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
      FAIL_ON_SEVERITY: "HIGH"
      ADDITIONAL_ARGS: "--detect.risk.report.pdf=TRUE"

  - docker build -t $IMAGE_NAME .

  - docker push $IMAGE_NAME

```

### Basic Docker image scan example

Uses Detect to scan a Docker image to find any vulnerabilities present.

```yaml
script:
  - docker build -t $IMAGE_NAME .

  - pipe: synopsys-sig/synopsys-detect:0.1.0
    variables:
      BLACKDUCK_SERVER_URL: $BLACKDUCK_SERVER_URL
      BLACKDUCK_ACCESS_TOKEN: $BLACKDUCK_ACCESS_TOKEN
      TOOLS: "DOCKER,SIGNATURE_SCAN"
      ADDITIONAL_ARGS: "--detect.docker.image=$IMAGE_NAME"

  - docker push $IMAGE_NAME
```

​

## Support

​
If you’d like help with this pipe, or you have an issue or feature request, please visit us at <https://community.synopsys.com/s/>

or if you’re reporting an [new issue](https://bitbucket.org/synopsys-sig/synopsys-detect/issues?status=new&status=open), please include:

* The version of the pipe
* Relevant logs and error messages
* Steps to reproduce

## License

​
Copyright (c) 2020 Synopsys, Inc. Apache 2.0 licensed, see LICENSE file.
