FROM gautambaghel/detect-dependencies:latest

COPY pipe /

ENTRYPOINT ["/pipe.sh"]